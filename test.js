var axios = require('axios');
var MongoClient = require('mongodb').MongoClient
var assert = require('assert');
const res = require('express/lib/response');
require('dotenv').config()

var url_db = process.env.URL_DB

MongoClient.connect(url_db, function(err,db) {
    assert.equal(null,err);
    console.log("Connected to server");
    const connect = db.db(process.env.NAME_DB)
    //connect.dropCollection("league")
    //connect.createCollection("league")
    //connect.dropCollection("teams")
    //connect.createCollection("teams")
    //connect.dropCollection("standings")
    //connect.createCollection("standings")
    //connect.dropCollection("stats")
    //connect.createCollection("stats")
    //connect.dropCollection("players")
   //connect.createCollection("players")
    //connect.dropCollection("top_scorer")
    //connect.createCollection("top_scorer")
    //connect.dropCollection("top_assist")
    //connect.createCollection("top_assist")
    connect.dropCollection("fixtures")
    connect.createCollection("fixtures")
    connect.dropCollection("fixtures_spe")
    connect.createCollection("fixtures_spe")
    
})

const stand = [];
const id_championnat = [];
id_championnat.push(61,135,140,78,39)
const date_season = [];
date_season.push(/*2015,2016,2017,2018,2019,2020,*/2021);

var id_teams_france = [] 
id_teams_france.push(77,78,79,80,81,82,83,84,85,91,93,94,95,97,99,106,110,112,116,1063)
var id_teams_france_page3 = []
id_teams_france_page3.push(77,78,79,81,84,85,91,93,94,99,110,112,1063)


const id_teams_espagne = []
id_teams_espagne.push(529,530,531,532,533,536,538,539,540,541,542,543,546,548,715,724,727,728,797,798)

const id_teams_allemagne = []
id_teams_allemagne.push(157,159,160,163,164,165,167,168,169,170,172,173,176,178,182,188,192)

const id_teams_italie = []
id_teams_italie.push(487,488,489,490,492,494,495,496,497,498,499,500,502,503,504,505,511,514,515,517)

const id_teams_angleterre = []
id_teams_angleterre.push(33,34,38,39,40,41,42,44,45,46,47,48,49,50,51,52,55,63,66,71)

// A TROUVER POUR RESOUDRE LE PROBLEME DES DOUBLONS



//////////////////////////////////////////////////// league ///////////////////////////////////////////////

/*league();

function league() {

  var id_championnat = [];
  id_championnat.push(61,135,140,78,39)

  id_championnat.forEach(element => {

    var config_leagues = {
      method: 'get',
      url: 'https://v3.football.api-sports.io/leagues?id='+element,
      headers: {
        'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
      }
    };
      
    console.log(config_leagues.url)
      
    axios(config_leagues) 
    .then(response => response.data.response).then(tab =>           
       {
        MongoClient.connect(url_db, function(err,db) {
            console.log("ceci est " + tab);
            console.log("")
            var dbo = db.db("football");
            console.log("Connected to server");
            tab.forEach(element => {
                dbo.collection("league").insertOne(element);  
                console.log("insertion");  
           })            
        })
    
    })
    
  })

}*/






//////////////////////////////////////// Teams ///////////////////////////////

/*
id_championnat.forEach(element => {
      var config_leagues = {
          method: 'get',
          url: 'https://v3.football.api-sports.io/teams?league='+element+'&season=2021',
          headers: {
            'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
          }
        };
          
        console.log(config_leagues.url)

        axios(config_leagues) 
        .then(response => response.data.response).then(tab =>           
        
         {
            MongoClient.connect(url_db, function(err,db) {
                console.log("ceci est " + tab);
                console.log("")
                var dbo = db.db("football");
                console.log("Connected to server");
                tab.forEach(element2 => {
                    dbo.collection("teams").insertOne(element2); 
                })
            })
         })
})
*/

    //////////////////////////////// Stats /////////////////////////////////
        
  /*
            id_teams_allemagne.forEach(item3 => {
    
                
                var config_leagues = {
                    method: 'get',
                    url: 'https://v3.football.api-sports.io/teams/statistics?league=78&season=2021&team='+item3,
                    headers: {
                      'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
                    }
                  };
                    
                  console.log(config_leagues.url)
                
        
                  axios(config_leagues) 
                  .then(response => response.data.response).then(tab =>           
                  
                   {
                      MongoClient.connect(url_db, function(err,db) {
                          console.log("")
                          var dbo = db.db("football");
                          console.log("Connected to server")
                          dbo.collection("stats").insertOne(tab); 
                          console.log("insertion");                                            
                              //dbo.collection("stats").insertOne(tab);  
                              //console.log("insertion");  
                                  
                      })
    
                      
                  
                  })
        
        })
*/

/*
        id_teams_espagne.forEach(item3 => {
    
                
          var config_leagues = {
              method: 'get',
              url: 'https://v3.football.api-sports.io/teams/statistics?league=140&season=2021&team='+item3,
              headers: {
                'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
              }
            };
              
            console.log(config_leagues.url)
          
  
            axios(config_leagues) 
            .then(response => response.data.response).then(tab =>           
            
             {
                MongoClient.connect(url_db, function(err,db) {
                    console.log("")
                    var dbo = db.db("football");
                    console.log("Connected to server")
                    dbo.collection("stats").insertOne(tab); 
                    console.log("insertion");                                            
                        //dbo.collection("stats").insertOne(tab);  
                        //console.log("insertion");  
                            
                })

                
            
            })
  
  })


  id_teams_angleterre.forEach(item3 => {
    
                
    var config_leagues = {
        method: 'get',
        url: 'https://v3.football.api-sports.io/teams/statistics?league=39&season=2021&team='+item3,
        headers: {
          'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
        }
      };
        
      console.log(config_leagues.url)
    

      axios(config_leagues) 
      .then(response => response.data.response).then(tab =>           
      
       {
          MongoClient.connect(url_db, function(err,db) {
              console.log("")
              var dbo = db.db("football");
              console.log("Connected to server")
              dbo.collection("stats").insertOne(tab); 
              console.log("insertion");                                            
                  //dbo.collection("stats").insertOne(tab);  
                  //console.log("insertion");  
                      
          })

          
      
      })

})


id_teams_allemagne.forEach(item3 => {
    
                
  var config_leagues = {
      method: 'get',
      url: 'https://v3.football.api-sports.io/teams/statistics?league=78&season=2021&team='+item3,
      headers: {
        'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
      }
    };
      
    console.log(config_leagues.url)
  

    axios(config_leagues) 
    .then(response => response.data.response).then(tab =>           
    
     {
        MongoClient.connect(url_db, function(err,db) {
            console.log("")
            var dbo = db.db("football");
            console.log("Connected to server")
            dbo.collection("stats").insertOne(tab); 
            console.log("insertion");                                            
                //dbo.collection("stats").insertOne(tab);  
                //console.log("insertion");  
                    
        })

        
    
    })

})


id_teams_italie.forEach(item3 => {
    
                
  var config_leagues = {
      method: 'get',
      url: 'https://v3.football.api-sports.io/teams/statistics?league=135&season=2021&team='+item3,
      headers: {
        'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
      }
    };
      
    console.log(config_leagues.url)
  

    axios(config_leagues) 
    .then(response => response.data.response).then(tab =>           
    
     {
        MongoClient.connect(url_db, function(err,db) {
            console.log("")
            var dbo = db.db("football");
            console.log("Connected to server")
            dbo.collection("stats").insertOne(tab); 
            console.log("insertion");                                            
                //dbo.collection("stats").insertOne(tab);  
                //console.log("insertion");  
                    
        })

        
    
    })

})


*/
////////////////////////////////////////// Standings ////////////////////////////////////////////////////////

/*
id_championnat.forEach(element => {
      var config_leagues = {
          method: 'get',
          url: 'https://v3.football.api-sports.io/standings?league='+element+'&season=2021',
          headers: {
            'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
          }
        };
          
        console.log(config_leagues.url)
     

        axios(config_leagues) 
        .then(response => response.data.response).then(tab =>           
        
         {
            MongoClient.connect(url_db, function(err,db) {
                console.log("ceci est " + tab);
                console.log("")
                var dbo = db.db("football");
                console.log("Connected to server");
                tab.forEach(element => {
                    dbo.collection("standings").insertOne(element);  
                    console.log("insertion");  
               })            
            })
        
        })
      })
*/


/*
         id_teams_france.forEach(item3 => {
    
          for(i=1;i<4;i++) {

            var config_leagues = {
              method: 'get',
              url: 'https://v3.football.api-sports.io/players?team='+item3+'&season=2021&page='+i,
              headers: {
                'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
              }
            };
              
            console.log(config_leagues.url)
        
  
            axios(config_leagues) 
            .then(response => response.data.response).then(tab =>           
            
             {
               if(tab=null) {
                 console.log("response null")
               }

               else {

                MongoClient.connect(url_db, function(err,db) {
                  console.log("")
                  var dbo = db.db("football");
                  console.log("Connected to server")
                  tab.forEach(element => 
                    {
                      dbo.collection("players").insertOne(element);  
                      console.log(element.statistics[0].team.name)
                      console.log(element.player.name)
                    })
                  
                                                           
                      //dbo.collection("stats").insertOne(tab);  
                      //console.log("insertion");  
                          
              })
               }
                

                
            
            })

          }
                
         
  
         })

*/
        /////////////////////////////////////////////////// Fixtures /////////////////////////////////////////


  id_championnat.forEach(element => {
        var config_leagues = {
          method: 'get',
          url: 'https://v3.football.api-sports.io/fixtures?league='+element+'&season=2021',
          headers: {
            'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
          }
        };
          
        console.log(config_leagues.url)

       

        axios(config_leagues) 
        .then(response => response.data.response).then(tab =>           
        
         {             
          MongoClient.connect(url_db, function(err,db) {
                console.log("ceci est " + tab);
                var dbo = db.db("football");
                console.log("Connected to server");
                tab.forEach(element => {
                  dbo.collection("fixtures").insertOne(element);
              
                  

  })

})

})

})
/*

function secondAxios(id, dbo) {

  var config_leagues2 = {
    method: 'get',
    url: 'https://v3.football.api-sports.io/fixtures?id='+id,
    headers: {
      'x-apisports-key': 'a6f8e98240e4127937a2bfb28e38de11'
    }
  };
                
  axios(config_leagues2) 
  .then(response2 => response2.data.response).then(tab2 => {
    tab2.forEach(elementT => {
      dbo.collection("fixtures_spe").insertOne(elementT);
      console.log("insertion")
    })
        
  }   

)}


*/